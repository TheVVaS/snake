js = {

    mapSize: 40,

    snakeHeadColor: '#FF0000',
    snakeEyesColor: '',
    snakeBodyColor: '#660000',
    pointColor: '#006600',
    wallColor: '',
    cellBorderColor: '#DDDDDD',
    cellBackgroundColor: '',

    snakeSpeed: 100,
    snakeMovementInterval: null,
    currentDirection: '',
    snake: [],
    map: [],

    gameOver: false,

    canvas: null,
    ctx: null,
    cellSize: [0,0],

    init: function () {

        // get canvas and it's context
        js.canvas = document.getElementById('play-screen');
        js.ctx = js.canvas.getContext('2d');

        // (re)start game
        js.firstTurn();
        js.actionPause();

        // set listeners on wanted events
        js.addEventListeners();
    },

    //// EVENTS

    addEventListeners: function () {

        // listen for movement changes
        document.addEventListener('keypress', js.updateSnakeMoveDirection, false);

        // start new game
        document.getElementById('game-start-new').addEventListener('click', js.firstTurn, false);

        // continue/pause
        document.getElementById('game-resume').addEventListener('click', js.startSnakeMovement, false);
        document.getElementById('game-pause').addEventListener('click', js.actionPause, false);

        // update snake speed
        document.getElementById('config-speed').addEventListener('input', js.updateSnakeSpeed, false);
        window.addEventListener('load', js.updateSnakeSpeed, false);

        // update map size
        document.getElementById('config-size').addEventListener('input', js.updateMapSize, false);
        window.addEventListener('load', js.updateMapSize, false);

        // on map resize
        window.addEventListener('resize', js.onMapResize, false);
        window.addEventListener('load', js.onMapResize, false);
    },

    //// MAP

    firstTurn: function () {

        // clear map
        js.map = [];

        // create all cells
        for (var x = 0; x < js.mapSize; x++) {

            // prepare row for cell states
            js.map[x] = [];

            // fill all cells
            for (var y = 0; y < js.mapSize; y++) {
                js.map[x][y] = '';
            }
        }

        // create new snake and make him move
        js.recreateSnake();
        js.createNewPoint();

        // draw all cells
        js.redrawCells();
    },

    nextTurn: function () {

        // move snake forward
        js.actionSnake();

        // redraw all cells
        js.redrawCells();
    },

    updateMapSize: function () {

        // get elements
        js.mapSize = document.getElementById('config-size').value;

        // update size label
        document.getElementById('config-size-label').innerText = 'Map size: ' + js.mapSize;

        // redraw map
        js.firstTurn();
    },

    onMapResize: function () {

        // width and height container of canvas
        var parentHeight = js.canvas.parentElement.clientHeight - 20;
        var parentWidth = js.canvas.parentElement.clientWidth - 20;

        // update map display size (let it be always square)
        js.canvas.style['width'] = js.canvas.width = ((parentWidth > parentHeight) ? parentHeight : parentWidth) - 2;
        js.canvas.style['height'] = js.canvas.height = ((parentWidth > parentHeight) ? parentHeight : parentWidth) - 2;

        // redraw cells with new size
        js.redrawCells();
    },

    //// SNAKE

    actionSnake: function () {

        // get snake's next position
        var nextPosition = js.getSnakeNextPosition();

        // get element on next position
        var isOutOfMap = (nextPosition[0] < 0 || nextPosition[1] < 0 || nextPosition[0] > js.mapSize-1 || nextPosition[1] > js.mapSize-1);
        var elementOnNextPosition = (isOutOfMap) ? 'endOfMap' : js.map[nextPosition[0]][nextPosition[1]];

        // if something is on next position
        if (elementOnNextPosition !== '') {

            // depending on what will collide with snake head do something
            switch (elementOnNextPosition)
            {
                case 'point':     js.eatPoint();       break;
                case 'wall':      js.actionGameOver(); break;
                case 'snakeBody': js.actionGameOver(); break;
                case 'endOfMap':  js.actionGameOver(); break;
            }
        }

        // make sure that move will be executed only when movement is enabled
        if (js.snakeMovementInterval !== null) js.moveSnake();
    },

    moveSnake: function () {

        // remove previous snake position from map
        js.snake.forEach(function (elementPosition) {
            js.map[elementPosition[0]][elementPosition[1]] = '';
        });

        // move snake body forward (start from last element)
        for (var i = js.snake.length-1; i >= 0; i--) {
            js.snake[i] = (i === 0) ? js.getSnakeNextPosition() : js.snake[i - 1];
        }

        // put snake current position into map
        js.snake.forEach(function (elementPosition, index) {
            js.map[elementPosition[0]][elementPosition[1]] = (index === 0) ? 'snakeHead' : 'snakeBody';
        });
    },

    getSnakeNextPosition: function () {

        // get snake's head current position
        var snakePosX = js.snake[0][0];
        var snakePosY = js.snake[0][1];

        // get next position depending on direction
        switch (js.currentDirection)
        {
            case 'left':  return [snakePosX-1, snakePosY  ];
            case 'up':    return [snakePosX,   snakePosY-1];
            case 'down':  return [snakePosX,   snakePosY+1];
            case 'right': return [snakePosX+1, snakePosY  ];
        }
    },

    recreateSnake: function () {

        // remove previous snake if exists
        js.snake = [];

        // create new snake head
        js.snake[0] = [1,1];

        // put snake head into map
        js.map[1][1] = 'snakeHead';

        // default direction
        js.currentDirection = 'down';

    },

    startSnakeMovement: function () {

        // start snake movement
        js.stopSnakeMovement();

        // mark pause as inactive
        document.getElementById('game-resume').style['color'] = 'red';
        document.getElementById('game-pause').style['color'] = '';

        // get snake movement speed and start movement
        js.snakeMovementInterval = setInterval(js.nextTurn, 10000 / js.snakeSpeed);
    },

    stopSnakeMovement: function () {

        // stop snake's movement
        clearTimeout(js.snakeMovementInterval);
        js.snakeMovementInterval = null;

        // mark pause as active
        document.getElementById('game-resume').style['color'] = '';
        document.getElementById('game-pause').style['color'] = 'red';
    },

    updateSnakeMoveDirection: function (event) {

        // disable moving in this direction
        var keyToGoBack = '';

        // prevent going back - only if snake have something more than head
        if (js.snake.length > 1) {

            // check what movement direction should be unable
            if (js.snake[0][0] - 1 === js.snake[1][0]) keyToGoBack = 'a';
            if (js.snake[0][0] + 1 === js.snake[1][0]) keyToGoBack = 'd';
            if (js.snake[0][1] - 1 === js.snake[1][1]) keyToGoBack = 'w';
            if (js.snake[0][1] + 1 === js.snake[1][1]) keyToGoBack = 's';

            // user tried change movement direction which was blocked
            if (event.key === keyToGoBack) return;
        }

        // get snake's new move direction
        switch (event.key)
        {
            case 'a': js.currentDirection = 'left';  break;
            case 'w': js.currentDirection = 'up';    break;
            case 's': js.currentDirection = 'down';  break;
            case 'd': js.currentDirection = 'right'; break;
        }
    },

    updateSnakeSpeed: function () {

        // get current speed
        var speed = document.getElementById('config-speed').value;

        // update snake speed
        js.snakeSpeed = speed;

        // reapply sneak speed only if currently moving
        if (js.snakeMovementInterval !== null) js.startSnakeMovement();

        // put new speed into label
        document.getElementById('config-speed-label').innerText = 'Speed: ' + speed/100;
    },

    //// ACTIONS

    eatPoint: function () {

        // get snake's next position
        var pointPosition = js.getSnakeNextPosition();

        // create new point in random position
        js.createNewPoint();

        // remove eaten point from map
        js.map[pointPosition[0]][pointPosition[1]] = '';

        // extend snake
        js.snake.push(js.snake[js.snake.length-1]);
    },

    createNewPoint: function () {

        while (true) {

            // get random position in map
            var xPos = Math.floor((Math.random() * js.mapSize));
            var yPos = Math.floor((Math.random() * js.mapSize));

            // if position is empty place there point
            if (js.map[xPos][yPos] === '') {
                js.map[xPos][yPos] = 'point';
                return;
            }
        }
    },

    actionGameOver: function () {

        // stop snake movement
        js.stopSnakeMovement();
    },

    actionPause: function () {

        // stop snake movement
        js.stopSnakeMovement();
    },

    //// DRAWING

    redrawCells: function () {

        // recalculate size of cells
        js.cellSize = [
            js.canvas.width / js.mapSize,
            js.canvas.height / js.mapSize
        ];

        // clear canvas
        js.clearPlayScreen();

        // redraw all cells
        for (var x = 0; x < js.mapSize; x++) {
            for (var y = 0; y < js.mapSize; y++) {
                js.redrawCell([x,y]);
            }
        }
    },

    redrawCell: function (position) {

        // calculate XY position of cell
        var xPos = position[0] * js.cellSize[0];
        var yPos = position[1] * js.cellSize[1];
        var element = js.map[position[0]][position[1]];

        // check if this cell contain something
        if (element !== '') {

            // what kind of element is ahead?
            switch (element)
            {
                case 'wall':      js.drawWall([xPos, yPos]);      break;
                case 'point':     js.drawPoint([xPos, yPos]);     break;
                case 'snakeHead': js.drawSnakeHead([xPos, yPos]); break;
                case 'snakeBody': js.drawSnakeBody([xPos, yPos]); break;
            }
        }

        // draw border
        js.drawBorder([xPos, yPos]);
    },

    drawWall: function (position) {

        // draw cell
        js.ctx.fillStyle = js.wallColor;
        js.ctx.fillRect(position[0], position[1], js.cellSize[0], js.cellSize[1]);
    },

    drawPoint: function (position) {

        // draw cell
        js.ctx.fillStyle = js.pointColor;
        js.ctx.fillRect(position[0], position[1], js.cellSize[0], js.cellSize[1]);
    },

    drawSnakeHead: function (position) {

        // draw cell
        js.ctx.fillStyle = js.snakeHeadColor;
        js.ctx.fillRect(position[0], position[1], js.cellSize[0], js.cellSize[1]);
    },

    drawSnakeBody: function (position) {

        // draw cell
        js.ctx.fillStyle = js.snakeBodyColor;
        js.ctx.fillRect(position[0], position[1], js.cellSize[0], js.cellSize[1]);
    },

    drawBorder: function (position) {

        // draw cell's border
        js.ctx.strokeStyle = js.cellBorderColor;
        js.ctx.strokeRect(position[0], position[1], js.cellSize[0], js.cellSize[1]);
    },

    clearPlayScreen: function () {
        js.ctx.clearRect(0, 0, js.canvas.width, js.canvas.height);
    }

};

js.init();